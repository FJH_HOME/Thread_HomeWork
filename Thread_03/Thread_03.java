package ThreadHome;

public class Thread_03 extends Thread{
	
	private int count=80;
	
	
	public void run()
	{
		while(this.getCount()>0)
		{
			this.send();
		}
	}
	
	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	public synchronized void send()
	{
		if(this.getCount()>0)
		{
			System.out.println(Thread.currentThread().getName()+"老师发放了第"+(81-count)+"本作业");
			this.setCount(this.getCount()-1);
		}else
		{
			System.out.println("作业发完了！！！");
		}
	}

}
