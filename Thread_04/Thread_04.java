package ThreadHome;

public class Thread_04 implements Runnable{
	
	private Plus p;
	
	private int a;
	private int b;

	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}
	

	public Thread_04(Plus p, int a, int b) {
		this.p = p;
		this.a = a;
		this.b = b;
	}
	public void run()
	{
		synchronized(p)
		{
			for(int i=this.getA();i<=this.getB();i++)
			{
				p.setSum(p.getSum()+i);
			}
		}
	}
	
	
	

}
